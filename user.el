(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-abort-on-unique-match nil)
 '(company-auto-commit nil)
 '(company-continue-commands nil)
 '(company-idle-delay 0.0)
 '(company-minimum-prefix-length 2)
 '(company-quick-access-modifier 'control)
 '(company-quickhelp-color-background "#111111")
 '(company-quickhelp-color-foreground "gainsboro")
 '(company-quickhelp-delay 0.7)
 '(company-quickhelp-mode t)
 '(company-quickhelp-use-propertized-text t)
 '(company-show-quick-access t)
 '(company-tooltip-align-annotations t)
 '(company-tooltip-idle-delay 0.0)
 '(company-tooltip-limit 15)
 '(company-tooltip-minimum 10)
 '(global-aggressive-indent-mode t)
 '(global-company-mode t)
 '(mode-line-default-help-echo 'mode-line-default-help-echo-1)
 '(package-selected-packages
   '(ht shackle ivy-posframe counsel swiper posframe undo-fu-session undo-fu smartparens sly-quicklisp sly-asdf rainbow-delimiters ag expand-region multiple-cursors sublime-themes magit slime-company company-quickhelp company sly slime neotree helpful doom-modeline doom-themes smex))
 '(portacle-ide 'sly)
 '(portacle-project-licence "BSD-3")
 '(portacle-setup-done-p t)
 '(posframe-mouse-banish nil)
 '(rainbow-blocks-highlight-braces-p nil)
 '(rainbow-blocks-highlight-brackets-p nil)
 '(rainbow-blocks-highlight-parens-p t)
 '(shift-select-mode t)
 '(show-paren-highlight-openparen t)
 '(show-paren-mode nil)
 '(show-paren-style 'mixed)
 '(show-paren-when-point-in-periphery nil)
 '(sly-db-focus-debugger 'always)
 '(sly-enable-evaluate-in-emacs t)
 '(sp-autoskip-closing-pair nil)
 '(user-full-name "Akasha Peppermint")
 '(user-mail-address "risole123@hotmail.com"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background nil))))
 '(company-tooltip-common ((t (:foreground "turquoise3"))))
 '(highlight-parentheses-highlight ((t (:underline t))) t)
 '(isearch ((t (:background "# 161a1f" :foreground "cyan" :weight bold))))
 '(lazy-highlight ((t (:background "#161a1f" :foreground "#FF80F4" :weight normal))))
 '(rainbow-blocks-depth-1-face ((t (:foreground "#b8e0e6"))))
 '(rainbow-blocks-depth-2-face ((t (:foreground "#d5e6b8"))))
 '(rainbow-blocks-depth-3-face ((t (:foreground "#b8e6cf"))))
 '(rainbow-blocks-depth-4-face ((t (:foreground "#e6b8e5"))))
 '(rainbow-blocks-depth-5-face ((t (:foreground "#e6b8c5"))))
 '(rainbow-blocks-depth-6-face ((t (:foreground "#d0b8e6"))))
 '(rainbow-blocks-depth-7-face ((t (:foreground "#b8c0e6"))))
 '(rainbow-blocks-depth-8-face ((t (:foreground "#e1d2b8"))))
 '(rainbow-blocks-depth-9-face ((t (:foreground "#e6bab8"))))
 '(rainbow-delimiters-base-error-face ((t (:inherit rainbow-delimiters-base-face :foreground "#88090B" :weight bold))))
 '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "gold"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "spring green"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "deep sky blue"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "light slate blue"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "violet"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "cyan"))))
 '(rainbow-delimiters-depth-9-face ((t (:inherit rainbow-delimiters-base-face :foreground "green yellow"))))
 '(rainbow-delimiters-mismatched-face ((t (:foreground "#e0211d" :overline t :weight bold))))
 '(rainbow-delimiters-unmatched-face ((t (:foreground "#e0211d" :strike-through "orange red" :overline t :weight ultra-bold)))))

(with-eval-after-load 'sly
  (add-to-list 'sly-contribs 'sly-asdf 'append)
  (add-to-list 'sly-contribs 'sly-quicklisp 'append)) 

(defun init-lisp-tools ()
  (sp-pair "`" nil :actions :rem)
  (sp-pair "'" nil :actions :rem))

(defun init-file-open ()
  (interactive)
  (load-file "user.el"))

(defun keybinds-file-open ()
  (interactive)
  (load-file "keybinds.el"))

(defun mode-line-default-help-echo-1 (window)
  "Return help echo text for WINDOW's mode line."
  (let* ((frame (window-frame window))
         (line-1a
          ;; Show text to select window only if the window is not
          ;; selected.
          (not (eq window (frame-selected-window frame))))
         (line-1b
          ;; Show text to drag mode line if either the window is not
          ;; at the bottom of its frame or the minibuffer window of
          ;; this frame can be resized.  This matches a corresponding
          ;; check in `mouse-drag-mode-line'.
          (or (not (window-at-side-p window 'bottom))
              (let ((mini-window (minibuffer-window frame)))
                (and (eq frame (window-frame mini-window))
                     (or (minibuffer-window-active-p mini-window)
                         (not resize-mini-windows))))))
         (line-2
          ;; Show text make window occupy the whole frame
          ;; only if it doesn't already do that.
          (not (eq window (frame-root-window frame)))))
    (when (or line-1a line-1b line-2)
      (concat
       (when (or line-1a line-1b)
         (concat
          "mouse-1: "
          (when line-1a "Select window")
          (when line-1b
            (if line-1a " (drag to resize)" "Drag to resize"))
          (when (or line-2) "\n")))
       (when line-2
         (concat
          "mouse-2: Make window occupy whole frame"))))))

(defun mouse-start-rectangle (start-event)
  (interactive "e")
  (deactivate-mark)
  (mouse-set-point start-event)
  (rectangle-mark-mode +1)
  ;; (cua-rectangle-mark-mode 1)
  ;; (cua-mouse-set-rectangle-mark start-event) 
  (let ((drag-event))
    (track-mouse     
      (while (progn    
               (setq drag-event (read-event))
               (mouse-movement-p drag-event))
        (mouse-set-point drag-event)
        ;; (cua-mouse-resize-rectangle drag-event)
        ))))
                            
(winner-mode)                 
(scroll-bar-mode)              
(global-undo-fu-session-mode)
(remove-hook 'lisp-mode-hook 'enable-paredit-mode)
(remove-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(remove-hook 'slime-repl-mode-hook 'enable-paredit-mode)
(remove-hook 'eval-expression-minibuffer-setup-hook 'enable-paredit-mode)
(remove-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
(remove-hook 'ielm-mode-hook 'enable-paredit-mode)
(add-hook #'prog-mode #'rainbow-blocks-mode)
(add-hook #'prog-mode #'rainbow-delimeters-mode)
(add-hook #'sly-mode #'rainbow-blocks-mode)
(add-hook #'sly-mode #'rainbow-delimeters-mode)

(defvar *ivy-keybinds-set* nil) 

(defun define-ivy-keybinds  ()
  (define-key swiper-map (kbd "C-r") 'swiper-query-replace)
  (define-key swiper-map (kbd "<up>") 'ivy-previous-line)
  (define-key swiper-map (kbd "<down>")  'ivy-next-line)
  (define-key swiper-map (kbd "C-S-f") 'ivy-previous-history-element)
  (define-key swiper-map (kbd "C-f") 'ivy-next-history-element))

;; Posframe is too buggy to use
;; (require 'ivy-posframe) 
;; (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-point)))
;; (ivy-posframe-mode 1)
;; Fix for weirdness with ivy posframe losing focus when clicking out of it
;; (company-posframe-mode 1)

;; It's emacs though so we just make our own
;; https://www.reddit.com/r/emacs/comments/b18vef/display_ivy_in_popup_window/
(defun ivy-display-function-window (text)
  (unless *ivy-keybinds-set* 
    (define-ivy-keybinds)
    (setq *ivy-keybinds-set* t))
  (let ((buffer (get-buffer-create "*ivy-candidate-window*"))
        (str (with-current-buffer (get-buffer-create " *Minibuf-1*")
               (let ((point (point))
                     (string (concat (buffer-string) "  " text)))
                 (add-text-properties (- point 1) point '(face cursor) string)
                 string))))
    (with-current-buffer buffer
      (let ((inhibit-read-only t))
        (erase-buffer)
        (insert str)))
    (with-ivy-window
      (display-buffer
       buffer
       `((display-buffer-reuse-window
          display-buffer-below-selected)
         (window-height . ,(ivy--height (ivy-state-caller ivy-last))))))))

(setq ivy-display-functions-alist
      '((counsel-M-x . ivy-display-function-window)
        (ivy-completion-in-region . ivy-display-function-overlay)
        ;; (t . ivy-posframe-display-at-point)
        (t . ivy-display-function-window)))

(load "move-marks.el")

(defun swiper--cleanup-and-mark-pos ()
  (call-interactively 'push-mark-ring)
  (swiper--cleanup))

(require 'ivy)

(defun custom-swiper ()
  (interactive)
  (ivy--alist-set 'ivy-unwind-fns-alist 'swiper #'swiper--cleanup-and-mark-pos) 
  (swiper))
(intellij-mark-mode 1)

(load "rebinder.el")
(load "deleto-dorito.el")
(load "keybinds.el")

(add-hook 'lisp-mode-hook 'smartparens-mode)
(add-hook 'lisp-interaction-mode-hook 'smartparens-mode)
(add-hook 'slime-repl-mode-hook 'smartparens-mode)
(add-hook 'eval-expression-minibuffer-setup-hook 'smartparens-mode)
(add-hook 'emacs-lisp-mode-hook 'smartparens-mode)
(add-hook 'ielm-mode-hook 'smartparens-mode)
(add-hook 'smartparens-enabled-hook #'init-lisp-tools)
;; (add-hook 'prog-mode-hook #'smartparens-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook #'rainbow-blocks-mode)
(add-hook 'prog-mode-hook #'global-aggressive-indent-mode)
(defun turn-off-portacle-keys ()
  (portacle-keys-minor-mode -1))
(add-hook 'lisp-mode-hook 'turn-off-portacle-keys)
(add-hook 'lisp-interaction-mode-hook 'turn-off-portacle-keys)
(add-hook 'slime-repl-mode-hook 'turn-off-portacle-keys)
(add-hook 'eval-expression-minibuffer-setup-hook 'turn-off-portacle-keys)
(add-hook 'emacs-lisp-mode-hook 'turn-off-portacle-keys)
(add-hook 'ielm-mode-hook 'turn-off-portacle-keys)

