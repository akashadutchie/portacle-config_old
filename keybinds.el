(defun sm-lambda-mode-hook ()
  (font-lock-add-keywords
   nil `(("\\<lambda\\>"
          (0 (progn (compose-region (match-beginning 0) (match-end 0)
                                    ,(make-char 'greek-iso8859-7
                                                107))
                    nil))))))

(ignore-errors
  (current-kill 0))


(add-hook 'prog-mode-hook 'sm-lambda-mode-hook)
;; how tf does anyone live without this?? so annoying accidentally closing buffers
(global-set-key [mode-line mouse-3] nil)
(global-unset-key (kbd "<mouse-3>"))

;; Suspect this bugs out company
(defun stop-using-minibuffer ()
  "kill the minibuffer"
  (when (and (>= (recursion-depth) 1) (active-minibuffer-window))
    (abort-recursive-edit)))

(add-hook 'mouse-leave-buffer-hook 'stop-using-minibuffer)

(global-set-key (kbd "<down-mouse-3>") #'mouse-start-rectangle)

(defun insert-lambda-from-greek ()
  (interactive)
  (insert "lambda "))
(global-set-key (kbd "λ") 'insert-lambda-from-greek)
(define-key key-translation-map (kbd "ESC") (kbd "C-g"))
(global-set-key (kbd "C-d") 'right-word)
(global-set-key (kbd "C-a") 'left-word)

(global-set-key (kbd "s-a") 'intellij-mark-ring-prev)
(global-set-key (kbd "s-d") 'intellij-mark-ring-next) 
(global-set-key (kbd "<C-next>") 'intellij-mark-ring-prev)
(global-set-key (kbd "<C-prior>") 'intellij-mark-ring-next) 

(global-set-key (kbd "M-a") 'sp-backward-sexp)
(global-set-key (kbd "M-d") 'sp-forward-sexp)


;; (define-key minibuffer-local-map (kbd "ESC") 'keyboard-escape-quit)

;; suspect causing company abort behaviour?
;; (global-set-key (kbd "s-w") 'windmove-up)
;; (global-set-key (kbd "s-s") 'windmove-down)
;; (global-set-key (kbd "s-a") 'windmove-left)
;; (global-set-key (kbd "s-d") 'windmove-right)

(define-key global-map [(control backspace)] 'dd-backward-delete-word-or-whitespace-or-sexp)
(define-key global-map [(control delete)] 'dd-forward-delete-word-or-whitespace-or-sexp)

(global-set-key (kbd "<M-delete>") 'sp-delete-hybrid-sexp)

(global-set-key (kbd "C-z") 'undo-fu-only-undo)
(global-set-key (kbd "C-S-z") 'undo-fu-only-redo)

(global-set-key (kbd "C-(") 'sp-wrap-round)
(global-set-key (kbd "C-)") 'sp-splice-sexp)
(global-set-key (kbd "C-M-(") 'sp-raise-sexp)
(global-set-key (kbd "C-M-)") 'sp-convolute-sexp)
(global-set-key (kbd "C-\"") 'sp-forward-slurp-sexp)
(global-set-key (kbd "C-:") 'sp-backward-slurp-sexp)
(global-set-key (kbd "M-(") 'sp-backward-barf-sexp)
(global-set-key (kbd "M-)") 'sp-forward-barf-sexp)

(define-key rebinder-mode-map (kbd "C-M-x") #'sp-backward-kill-sexp)
(define-key rebinder-mode-map (kbd "C-M-c") 'sp-backward-copy-sexp)

;;(rebinder-hook-to-mode 'my-mode 'my-mode-hook)
(global-set-key (kbd "C-o") #'find-file)
(global-set-key (kbd "C-v") #'dd-inject-yank)
;; (global-set-key (kbd "C-S-v") #'yank-pop)
(global-set-key (kbd "C-S-v") #'yank)
(global-set-key (kbd "C-b") 'sp-clone-sexp)
;; theres no way im going to remember these though
;; (global-set-key (kbd "C-S-a") 'sp-backward-up-sexp)
;; (global-set-key (kbd "C-S-d") 'sp-down-sexp)

(defun backspace ()
  (interactive)
  (delete-backward-char 1))
(global-set-key (kbd "<backspace>") #'backspace)
;;(global-set-key (kbd "<C-escape>") 'sly-db-invoke-restart-0)
;;(global-set-key (kbd "<S-escape>") 'sly-db-abort)
;;(define-key sly-mrepl-mode-map (kbd "q") 'sly-db-abort)
;; (global-set-key (kbd "C-1") 'sly-db-invoke-restart-1)
;; (global-set-key (kbd "C-2") 'sly-db-invoke-restart-2)
;; (global-set-key (kbd "C-3") 'sly-db-invoke-restart-3)
;; (global-set-key (kbd "C-4") 'sly-db-invoke-restart-4)
;; (global-set-key (kbd "C-5") 'sly-db-invoke-restart-5)
;; (global-set-key (kbd "C-6") 'sly-db-invoke-restart-6)
;; (global-set-key (kbd "C-7") 'sly-db-invoke-restart-7)
;; (global-set-key (kbd "C-8") 'sly-db-invoke-restart-8)
;; (global-set-key (kbd "C-9") 'sly-db-invoke-restart-9)

(global-set-key (kbd "<C-S-f4>") 'kill-current-buffer)
(global-set-key (kbd "<C-f4>") 'delete-window)
(global-set-key (kbd "<M-tab>") #'switch-to-buffer)

(global-set-key (kbd "C-q") 'sly-compile-defun)
(global-set-key (kbd "C-S-q") 'sly-compile-region)

;; Never mind.
;; (defvar *swiper-something-typed* nil)
;; (defun swiper-reset ()
;;   (setq *swiper-something-typed* nil))
;; (defun swiper-move ()
;;   (if (member 'ivy-mode minor-mode-list)
;;       (setq *swiper-something-typed* nil)))
;; (add-hook 'ivy-mode-hook #'swiper-reset)
;; (defun pam ()
;;   (interactive)
;;   (message "Active modes are %s" (member 'ivy-mode minor-mode-list)))

(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "C-S-f") 'isearch-repeat-backward) 
(global-set-key (kbd "C-f") 'custom-swiper) 
(global-set-key (kbd "C-S-f") 'swiper-thing-at-point) 
(global-set-key (kbd "M-f") 'isearch-forward) 
(global-set-key (kbd "M-S-f") 'isearch-backward)

(global-set-key (kbd "C-r") 'query-replace)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-S-s") 'save-some-buffers)
(define-key global-map (kbd "M-q") (rebinder-dynamic-binding "C-x"))
(define-key global-map (kbd "C-w") (rebinder-dynamic-binding "C-c"))
(define-key rebinder-mode-map (kbd "C-S-c") 'comment-line)
(define-key rebinder-mode-map (kbd "C-y") 'kill-whole-line)
(define-key rebinder-mode-map (kbd "C-x") 'dd-kill-sexp-or-selection)
(define-key rebinder-mode-map (kbd "C-S-x") 'dd-delete-sexp-or-selection)
(define-key rebinder-mode-map (kbd "C-c") 'dd-copy-sexp-or-selection)
(rebinder-hook-to-mode t 'after-change-major-mode-hook)

(global-set-key (kbd "C-t") 'winner-undo)
(global-set-key (kbd "C-S-t") 'winner-redo)
;; (global-set-key (kbd "<M-prior>") 'sly-edit-definition)
;; (global-set-key (kbd "<M-next>") 'pop-tag-mark)
;;(global-set-key (kbd "<M-mouse-1>") 'sly-edit-definition)

;; SLY config
(global-set-key (kbd "<XF86Launch6>") 'sly-edit-definition)
(global-set-key (kbd "<C-XF86Launch6>") 'xref-find-definitions)
(global-set-key (kbd "<XF86Launch7>") 'sly-pop-find-definition-stack)
(global-set-key (kbd "C-l") 'sly-quickload)
(global-set-key (kbd "s-s") 'sly-macroexpand-1)
(global-set-key (kbd "<XF86Launch8>") 'sly-macroexpand-1)
(global-set-key (kbd "<XF86Launch5>")
                (lambda ()
                  (interactive)
                  (sly-eval-for-inspector
                   `(slynk:init-inspector
                     ,(sly-sexp-at-point 'interactive nil nil))
                   :inspector-name nil)))

(add-hook 'sly-mrepl-mode-hook (lambda ()
                                 ;; Due to a bug, smartparens needs to be set on here explicitly
                                 (turn-on-smartparens-mode)
                                 (rainbow-blocks-mode-enable)
                                 (rainbow-delimiters-mode-enable)
                                 ;; the bug is that if we use (global-smartparens-mode) it bugs out company-capf completely
                                 (define-key sly-mrepl-mode-map (kbd "<S-return>") #'newline)
                                 
                                 (define-key sly-mrepl-mode-map (kbd "C-f") 'comint-history-isearch-backward)
                                 (define-key sly-mrepl-mode-map (kbd ",") nil)
                                 (define-key sly-mrepl-mode-map (kbd "C-S-f") 'helm-comint-prompts)
                                 (define-key sly-mrepl-mode-map (kbd "<tab>") 'comint-previous-input)
                                 (define-key sly-mrepl-mode-map (kbd "<backtab>") 'comint-next-input)
                                 ;;(unless (sly-eval '(slynk-stickers:toggle-break-on-stickers))
                                 ;;  (sly-eval '(slynk-stickers:toggle-break-on-stickers)))
                                 ))

;;These need to be here for some reason or else company doesnt show up at all, but they don't actually do anything
;; (define-key company-active-map (kbd "\C-s") 'company-show-doc-buffer)
;; (define-key company-active-map (kbd "C-e") 'company-show-location)


